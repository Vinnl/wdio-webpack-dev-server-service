# Changelog

This project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [2.0.2] - 2018-08-07

### Bugfixes

- Removed unnecessary peerDependency on http-proxy-middleware.

## [2.0.1] - 2018-08-03

### New features

- None, really - I just accidentally released 2.0.0 under the `next` label when setting up CI/CD scripts.

## [2.0.0] - 2018-08-03

### New features

- Support for Webpack 4 (#3)
- Support for webpack-serve

### Breaking changes

- Webpack 3 and lower are no longer supported
- Node versions lower than 8 are no longer supported
- This project is now based on webpack-serve rather than webpack-dev-server. webpack-serve can be configured by [adding the `serve` property to your Webpack config](https://github.com/webpack-contrib/webpack-serve#webpack-config-serve-property).
- You now only need to [configure your proxy for webpack-serve](https://github.com/webpack-contrib/webpack-serve#add-function-parameters) once; it will then automatically be picked up by wdio-webpack-dev-server-service. An example proxy configuration can be found here: https://gitlab.com/Vinnl/wdio-webpack-dev-server-service/issues/3#note_92742652

## [1.2.0] - 2018-01-11

https://gitlab.com/Vinnl/wdio-webpack-dev-server-service/compare/v1.1.0...v1.2.0

### New features

- Adopt `stats` configuration for the dev server as well. Merge request !2, thanks @fernandopasik!
- Enable configuring a proxy server to redirect calls to e.g. API's when developing locally. Merge request !3, thanks @Josh-ES!

### Bug fixes

- Don't error when the `devServer` config is `null`. Merge request !1, thanks @Narazaka!

## [1.1.0] - 2017-08-04

https://gitlab.com/Vinnl/wdio-webpack-dev-server-service/compare/v1.0.0...v1.1.0

### New features

- Mark as compatible with Webpack 3 in addition to Webpack 2 (to allow deduplication in dependent projects)

