const serve = require("webpack-serve");

class WDSLauncher {
  async onPrepare(config) {
    if(!config.webpackConfig) {
        const path = require('path');
        config.webpackConfig = path.resolve(process.cwd(), 'webpack.config.js');
    }

    const webpackConfig = typeof config.webpackConfig === 'string' ? require(config.webpackConfig) : config.webpackConfig;
    const webpackPort = config.webpackPort || 8080;

    const argv = {};
    this.serve = await serve(
      argv,
      {
        ...webpackConfig.serve,
        config: webpackConfig,
        port: webpackPort,
      },
    );
  }

  onComplete() {
    this.serve.app.stop()
  }
}

module.exports = WDSLauncher
