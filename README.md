WDIO Webpack Dev Server Service
======
This service starts Webpack Dev Server for you to run [WebdriverIO](https://webdriver.io/) against.

## Installation

```bash
npm install wdio-webpack-dev-server-service --save-dev
```

## Configuration

This assumes you have:

- [set up WebdriverIO](https://webdriver.io/guide.html).
- [set up Webpack Serve](https://github.com/webpack-contrib/webpack-serve) - the example will use `webpack.dev.config.js` as the configuration file.

**Note:** If you need support for webpack-dev-server (which is in maintenance mode) and Webpack 2 or 3, use `webpack-dev-server-service@1.2.0`.

Add the Webpack Dev Server service to your WebdriverIO services list:

```js
// wdio.conf.js
export.config = {
  // ...
  services: ['webpack-dev-server'],
  // ...
};
```

Options are set directly on your WebdriverIO config as well, e.g.

```js
// wdio.conf.js
export.config = {
  // ...
  webpackConfig: require('webpack.dev.config.js'),
  webpackPort: 8080,
  // ...
};
```

## Options

### `webpackConfig`
Type: `String` | `Function` | `Object`

Default: `webpack.config.js`

Either the absolute path to your Webpack configuration, or (a function generating) your actual Webpack configuration.

### `webpackPort`
Type: Number

Default: `8080`

The port the Dev Server should be run on. You'll want to set this same port in the `baseUrl` option of your WebdriverIO configuration.
